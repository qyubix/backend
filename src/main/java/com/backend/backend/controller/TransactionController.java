package com.backend.backend.controller;

import com.backend.backend.dao.BuyerDao;
import com.backend.backend.dao.ProductDao;
import com.backend.backend.dao.TransactionDao;
import com.backend.backend.json.JsonTransaction;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by AnsariMelah on 01/08/2017.
 */

@RestController
public class TransactionController {

    JsonTransaction jsonTransaction = new JsonTransaction();

    @Autowired
    private TransactionDao transaction;

    @Autowired
    private ProductDao product;

    @Autowired
    private BuyerDao buyer;



    @CrossOrigin
    @Async
    @RequestMapping(
            value = "/transaction",
            params = {"userId","transactionStatus","limit"},
            method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public JSONObject findTransactionByUserId(@RequestParam("userId") String userId,
                                              @RequestParam("transactionStatus") String transactionStatus,
                                              @RequestParam("limit") int limit){
        List[] transactionList=transaction
                .findTransactionAndProductAndStoreKeyByUserIdAndTransactionStatus(userId, transactionStatus, limit);
        return jsonTransaction.getTransactionAndProductAndStoreKeyByUserIdAndTransactionStatus(transactionList);
    }

    @CrossOrigin
    @Async
    @RequestMapping(
            value = "/transaction",
            params = {"userId","transactionStatus","size","number","limit"},
            method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public JSONObject findTransactionAndProductAndStoreKeyByUserIdAndTransactionStatus(
            @RequestParam("userId") String userId,
            @RequestParam("transactionStatus") String transactionStatus,
            @RequestParam("size") int size,
            @RequestParam("number") int number,
            @RequestParam("limit") int limit){
        List[] transactionList= transaction
                .findTransactionAndProductAndStoreKeyByUserIdAndTransactionStatus(userId, transactionStatus, limit);
        return jsonTransaction
                .getTransactionAndProductAndStoreKeyByUserIdAndTransactionStatusWithPagination
                        (transactionList,size,number);
    }

    @CrossOrigin
    @Async
    @RequestMapping(
            value = "/transactionGetAllRelationship",
            params = {"userId"},
            method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public JSONObject findTransactionWithAllRelationshipByUserId(@RequestParam("userId") String userId){
        List[] transactionList= transaction.findTransactionWithAllRelationshipByUserId(userId);
        return jsonTransaction.getTransactionWithAllRelationshipByUserId(transactionList);
    }

    @CrossOrigin
    @RequestMapping(value = "/transaction", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void insertNewProduct(@RequestBody JSONObject o){
        UUID randomId = UUID.randomUUID();
        transaction.insertNewTransaction(
                randomId.toString(),
                new Date(),
                Integer.parseInt(o.get("transaction_quantity").toString()),
                o.get("transaction_status").toString(),
                o.get("transaction_buyer_id").toString(),
                o.get("transaction_product_id").toString());
    }
}
