package com.backend.backend.controller;

import com.backend.backend.dao.ProductDao;
import com.backend.backend.json.JsonProduct;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
public class ProductController {


    JsonProduct jsonProduct =new JsonProduct();

    @Autowired
    private ProductDao product;

    //GET METHOD========================================================================================================

    @CrossOrigin
    @Async
    @RequestMapping(value = "/product", params = {"productCategory","limit"},method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public JSONObject findProductByProductCategory(@RequestParam("productCategory") String productCategory,
                                                  @RequestParam("limit") int limit){
        List[] productDetailList=product.findIdProductNameProductPriceStoreKey(productCategory,limit);
        return jsonProduct.getIdProductNameProductPriceStoreKey(productDetailList);
    }

    @CrossOrigin
    @Async
    @RequestMapping(value = "/product", params =  {"productCategory","limit","size","number"},method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public JSONObject findProductByProductCategoryWithPagination(@RequestParam("productCategory") String productCategory,
                                                              @RequestParam("limit") int limit,
                                                              @RequestParam("size") int size,
                                                              @RequestParam("number") int number){
        List[] productDetailList=product.findComplete(productCategory,limit);
        return jsonProduct.getFullProductWithPagination(productDetailList,size,number);
    }

    @CrossOrigin
    @Async
    @RequestMapping(value = "/product", params =  {"productName","limit","size","number"},method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public JSONObject findProductByProductNameWithPagination(@RequestParam("productName") String productName,
                                                                 @RequestParam("limit") int limit,
                                                                 @RequestParam("size") int size,
                                                                 @RequestParam("number") int number){
        List[] productDetailList=product.findProductByProductNameWithPagination(productName,limit);
        return jsonProduct.getFullProductWithPagination(productDetailList,size,number);
    }

    @RequestMapping(value = "/product/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @CrossOrigin
    public JSONObject findById(@PathVariable("id") String id)throws IOException {
        List productDetail[]=product.findCompleteById(id);
        return jsonProduct.getFullProduct(productDetail);
    }

    @CrossOrigin
    @RequestMapping(value = "/productsearch",method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public JSONObject findIdNameStore(@RequestParam("limit") int limit){
            List productDetail[]=product.findIdNameStore(limit);
        return jsonProduct.getIdNameStore(productDetail);
    }

//    @CrossOrigin
//    @Async
//    @RequestMapping(value = "/findProductByOneParam",method = RequestMethod.GET)
//    @ResponseStatus(HttpStatus.OK)
//    public Page<Product> findProductByOneParam(
//            @RequestParam(value = "pageNumber") int pageNumber,
//            @RequestParam(value = "size") int size,
//            @RequestParam(value = "limit", defaultValue = "1") int limit,
//            @RequestParam(value = "id", defaultValue = "unavailable") String id,
//            @RequestParam(value = "productStoreKey", defaultValue = "unavailable") String productStoreKey,
//            @RequestParam(value = "productPrice", defaultValue = "unavailable") String productPrice,
//            @RequestParam(value = "productDescription", defaultValue = "unavailable") String productDescription,
//            @RequestParam(value = "productSold", defaultValue = "unavailable") String productSold,
//            @RequestParam(value = "productImage_total", defaultValue = "unavailable") String productImage_total,
//            @RequestParam(value = "productName", defaultValue = "unavailable") String productName,
//            @RequestParam(value = "productCategory", defaultValue = "unavailable") String productCategory,
//            @RequestParam(value = "productDiscount_after", defaultValue = "unavailable") String productRating,
//            @RequestParam(value = "productRating", defaultValue = "unavailable") String product_rating
//    ){
//
//        PageRequest reqpage = new PageRequest(pageNumber,size);
//        return product.findTop9ByProductCategory(productCategory,reqpage);
//
//    }
//
//    @CrossOrigin
//    @RequestMapping(value = "/product", params = "product_discount", method = RequestMethod.GET)
//    @ResponseStatus(HttpStatus.OK)
//    public Page<Product> findByDiscount(@RequestParam("product_discount") int productDiscount){
//        PageRequest page = new PageRequest(0,5);
//        return product.findByProductDiscount(productDiscount, page);
//    }
//
//    @CrossOrigin
//    @RequestMapping(value = "/product", params = "product_discount_after", method = RequestMethod.GET)
//    @ResponseStatus(HttpStatus.OK)
//    public Page<Product> findByDiscountAfter(@RequestParam("product_discount_after") int productDiscountAfter){
//        PageRequest page = new PageRequest(0,5);
//        return product.findByProductDiscountAfter(productDiscountAfter, page);
//    }
//
//    @CrossOrigin
//    @RequestMapping(value = "/product", params = "product_store", method = RequestMethod.GET)
//    @ResponseStatus(HttpStatus.OK)
//    public Page<String> findByStoreId(@RequestParam("product_store") String id){
//        PageRequest page = new PageRequest(0,5);
//        return product.findByProductStoreId(id, page);
//    }
//
//    @RequestMapping(value = "/product", params = {"product_name","size","page"}, method = RequestMethod.GET)
//    @ResponseStatus(HttpStatus.OK)
//    @CrossOrigin
//    public Page<Product> findByName(@RequestParam("product_name") String productName,
//                                    @RequestParam("size") String size,
//                                    @RequestParam("page") String page){
//        PageRequest reqpage = new PageRequest(Integer.parseInt(page),Integer.parseInt(size));
//        return product.findByProductName(productName, reqpage);
//    }
//
//    @CrossOrigin
//    @RequestMapping(value = "/product", params = "product_price", method = RequestMethod.GET)
//    @ResponseStatus(HttpStatus.OK)
//    public Page<Product> findByPrice(@RequestParam("product_price") int productPrice){
//        PageRequest page = new PageRequest(0,5);
//        return product.findByProductPrice(productPrice, page);
//    }
//
//    @RequestMapping(value = "/product", params = "product_rating", method = RequestMethod.GET)
//    @ResponseStatus(HttpStatus.OK) @CrossOrigin
//    public Page<Product> findByRating(@RequestParam("product_rating") int productRating){
//        PageRequest page = new PageRequest(0,5);
//        return product.findByProductRating(productRating, page);
//    }
//
//    @RequestMapping(value = "/product", params = "product_sold", method = RequestMethod.GET)
//    @ResponseStatus(HttpStatus.OK) @CrossOrigin
//    public Page<Product> findBySold(@RequestParam("product_sold") int productSold){
//        PageRequest page = new PageRequest(0,5);
//            return product.findByProductSold(productSold, page);
//    }
//
//    @RequestMapping(value = "/product", params = "product_description", method = RequestMethod.GET)
//    @ResponseStatus(HttpStatus.OK) @CrossOrigin
//    public Page<Product> findByDescription(@RequestParam("product_description") java.lang.String productDescription){
//        PageRequest page = new PageRequest(0,5);
//        return product.findByProductDescription(productDescription, page);
//    }
//
//    @RequestMapping(value = "/product", params = "product_image_total", method = RequestMethod.GET)
//    @ResponseStatus(HttpStatus.OK)
//    @CrossOrigin
//    public Page<Product> findByImageTotal(@RequestParam("product_image_total") int productImageTotal){
//        PageRequest page = new PageRequest(0,5);
//        return product.findByProductImageTotal(productImageTotal, page);
//    }
//
//
//
//    //    @CrossOrigin
////    @RequestMapping(value = "/product", params = {"product_category","size","page"}, method = RequestMethod.GET)
////    @ResponseStatus(HttpStatus.OK)
////    public Page<Product> findByCategory(@RequestParam("product_category") String productCategory,
////                                        @RequestParam("size") String size,
////                                        @RequestParam("page") String page) {
////        PageRequest reqpage = new PageRequest(Integer.parseInt(page),Integer.parseInt(size));
////        return product.findByProductCategory(productCategory, reqpage);
////    }
//
//
//    //POST METHOD=======================================================================================================
//        @RequestMapping(value = "/product", method = RequestMethod.POST)
//        @ResponseStatus(HttpStatus.CREATED)
//        public void insertNewProduct(@RequestBody Product p){
//            product.save(p);
//        }
//
//    //PUT METHOD========================================================================================================
//    @RequestMapping(value = "/product/{id}", method = RequestMethod.PUT)
//    @ResponseStatus(HttpStatus.OK)
//    public void updateProduct(@PathVariable("id") String id, @RequestBody Product p){
//        p.setId(id);
//        product.save(p);
//    }
//
//    //DELETE METHOD=====================================================================================================
//    @RequestMapping(value = "/product/{id}", method = RequestMethod.DELETE)
//    @ResponseStatus(HttpStatus.OK)
//    public void deleteProduct(@PathVariable("id") String id){
//        product.delete(id);
//    }
}
