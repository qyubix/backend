package com.backend.backend.controller;

import com.backend.backend.dao.BuyerDao;
import com.backend.backend.dao.UserDao;
import com.backend.backend.entity.Buyer;
import com.backend.backend.entity.User;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

/**
 * Created by AnsariMelah on 08/08/2017.
 */
@RestController
public class BuyerController {

    @Autowired
    private BuyerDao buyer;

    @RequestMapping("/buyer")
    @CrossOrigin
    public Page<Buyer> getAllUser(Pageable page){
        return buyer.findAll(page);
    }

    @RequestMapping(value = "/buyer/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @CrossOrigin
    public ResponseEntity<Buyer> findById(@PathVariable("id") String id){
        Buyer idResult = buyer.findOne(id);
        if(idResult==null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(idResult, HttpStatus.OK);
    }

    @RequestMapping(value = "/dashboardlogin", method = RequestMethod.POST)
    @CrossOrigin
    public String login(@RequestBody Buyer login) throws ServletException, NoSuchAlgorithmException {
        String jwtToken = "";

        if (login.getBuyerName() == null || login.getBuyerPassword() == null) {
            throw new ServletException("Please fill in username and password");
        }

        String username = login.getBuyerName();
        String password = login.getBuyerPassword();

        String buyerByPassword = buyer.findIdByUserNameAndPassword(username,sha1(password));

        if (buyerByPassword == null) {
            throw new ServletException("User or Passwor Not Correct!!.");
        }

        jwtToken = Jwts.builder().setSubject(username).setExpiration(new Date(System.currentTimeMillis() + 10000000))
                .claim("role", "2").setIssuedAt(new Date())
                .signWith(SignatureAlgorithm.HS256, "secretkey").compact();

        jwtToken = "{\"token\": \"" + jwtToken + "\","+"\"key\": \""+buyerByPassword+"\" , \"role\":\"2\"}";
        return jwtToken;
    }
    //POST METHOD=======================================================================================================
    //PUT METHOD========================================================================================================
    //DELETE METHOD=====================================================================================================

    static String sha1(String input) throws NoSuchAlgorithmException {
        MessageDigest mDigest = MessageDigest.getInstance("SHA1");
        byte[] result = mDigest.digest(input.getBytes());
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < result.length; i++) {
            sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }
}
