package com.backend.backend.controller;

import com.backend.backend.dao.UserDao;
import com.backend.backend.entity.User;
import com.backend.backend.json.JsonUser;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;

@RestController
public class UserController {

    JsonUser jsonUser =new JsonUser();

    @Autowired
    private UserDao user;

    @RequestMapping(value = "/user", params = {"limit"},method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @CrossOrigin
    public JSONObject findAllUser(@RequestParam("limit") int limit){
        List[] userList=user.findAllUser(limit);
        return jsonUser.getAllUser(userList);
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @CrossOrigin
    public JSONObject findUserById(@PathVariable("id") String id){
        List[] userList=user.findUserById(id);
        return jsonUser.getAllUser(userList);
    }

    @CrossOrigin
    @Async
    @RequestMapping(
            value = "/user/valuableuser",
            params = {"limit"},
            method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public JSONObject findTheMostDiscountGiver(@RequestParam("limit") int userId){
        List[] transactionList= user.findTheMostDiscountGiver(userId);
        return jsonUser.findTheMostDiscountGiver(transactionList);
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @CrossOrigin
    public String login(@RequestBody User login) throws ServletException, NoSuchAlgorithmException {
        String jwtToken = "";

        if (login.getUserName() == null || login.getUserPassword() == null) {
            throw new ServletException("Please fill in username and password");
        }

        String username = login.getUserName();
        String password = login.getUserPassword();

        String userByPassword = user.findIdByUserNameAndPassword(username,sha1(password));

        if (userByPassword == null) {
            throw new ServletException("User or Passwor Not Correct!!.");
        }

        jwtToken = Jwts.builder().setSubject(username).setExpiration(new Date(System.currentTimeMillis() + 300000))
                .claim("role", "ROLE_ADMIN").setIssuedAt(new Date())
                .signWith(SignatureAlgorithm.HS256, "secretkey").compact();

        jwtToken = "{\"token\": \"" + jwtToken + "\","+"\"key\": \""+userByPassword+"\" , \"role\":\"1\"}";
        return jwtToken;
    }

    //POST METHOD=======================================================================================================
    //PUT METHOD========================================================================================================
    //DELETE METHOD=====================================================================================================

    static String sha1(String input) throws NoSuchAlgorithmException {
        MessageDigest mDigest = MessageDigest.getInstance("SHA1");
        byte[] result = mDigest.digest(input.getBytes());
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < result.length; i++) {
            sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }
}
