package com.backend.backend.datainjector.filesys;

import com.backend.backend.dao.BuyerDao;
import com.backend.backend.dao.ProductDao;
import com.backend.backend.dao.StoreDao;
import com.backend.backend.dao.UserDao;
import com.backend.backend.datainjector.dao.InjectBuyer;
import com.backend.backend.datainjector.dao.InjectStore;
import com.backend.backend.datainjector.dao.InjectTransaction;
import com.backend.backend.datainjector.dao.InjectUser;
import com.backend.backend.entity.Buyer;
import com.backend.backend.entity.Product;
import com.backend.backend.entity.Store;

import com.backend.backend.entity.User;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.sql.*;
import javax.sql.DataSource;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Random;

@RestController
public class InjectFolder {

    @Autowired
    private StoreDao store;

    @Autowired
    private ProductDao product;

    @Autowired
    private UserDao user;

    @Autowired
    private BuyerDao buyer;

    @Autowired
    private DataSource ds;

    private static final Logger log = LoggerFactory.getLogger(InjectFolder.class);
    public static final String images = System.getProperty("user.dir")+"/images";
    public static final String fakeimages = System.getProperty("user.dir")+"/fakeimages";

    Random random = new Random();

    @CrossOrigin
    @RequestMapping(value = "/createfolder",method = RequestMethod.GET)
    public String createFolder() throws IOException {
        System.out.println("Cleaning directory : "+images);
        FileUtils.cleanDirectory(new File(images));

        for (Store model:store.findAll()) {
            System.out.println(model.getStoreKey());
            Path path1 = Paths.get(images+"/product/"+model.getStoreKey());
            Files.createDirectories(path1);
        }

        Path path2 = Paths.get(images+"/user");
        Path path3 = Paths.get(images+"/buyer");

        Files.createDirectories(path2);
        Files.createDirectories(path3);
        return "Success create folder";
    }

    @CrossOrigin
    @RequestMapping(value = "/uploadimage",method = RequestMethod.GET)
    public String uploadImage() throws IOException, SQLException {
        for (Product model:product.findAll()) {
            String mId       = model.getId();
            String mStoreKey  = model.getProductStore().getStoreKey();
            int mImageTotal = model.getProductImageTotal();
            String mCategory;
            System.out.println("imagetotal :"+mImageTotal);

            Path path = Paths.get(images+"/product/"+mStoreKey+"/"+mId);
            Files.createDirectories(path);
            try{
                mCategory= store.findStoreProductByStoreKey(mStoreKey);
            }catch (Exception e){
                continue;
            }

            for(int i = 1; i<=mImageTotal;i++) {
                System.out.println("ID : " + mId +
                        "\nCategory : " + mCategory +
                        "\nProduct store ID : " + mStoreKey);

                File resource = new File(fakeimages + "/" + mCategory.replaceAll("\\W.*", "") + "/" + (random.nextInt(5) + 1) + ".jpg");
                File destination = new File(images + "/product/" + mStoreKey + "/" + mId + "/"+i+".jpg");
                System.out.println("Resource : " + resource.getAbsoluteFile() + "\nDestination : " + destination.getAbsoluteFile());

                if (!resource.exists()) {
                    resource = new File(fakeimages + "/sport/" + (random.nextInt(5) + 1) + ".jpg");
                }
                Files.copy(resource.toPath(), destination.toPath());
            }
        }
        return "Success upload product images";
    }

    @CrossOrigin
    @RequestMapping(value = "/uploaduserimage",method = RequestMethod.GET)
    public String uploadUserImage() throws IOException, SQLException {
        for (User model:user.findAll()) {
            String mId       = model.getId();

            Path path = Paths.get(images+"/user/"+mId);
            Files.createDirectories(path);


                File resource = new File(fakeimages + "/USER/" + (random.nextInt(25) + 1) + ".jpg");
                File destination = new File(images + "/user/" + mId + "/" + mId + ".jpg");
                System.out.println("Resource : " + resource.getAbsoluteFile() + "\nDestination : " + destination.getAbsoluteFile());

                if (!resource.exists()) {
                    resource = new File(fakeimages + "/USER/1.jpg");
                }
                Files.copy(resource.toPath(), destination.toPath());
        }
        return "Success upload user images";
    }

    @CrossOrigin
    @RequestMapping(value = "/uploadbuyerimage",method = RequestMethod.GET)
    public String uploadBuyerImage() throws IOException, SQLException {
        for (Buyer model:buyer.findAll()) {
            String mId       = model.getId();

            Path path = Paths.get(images+"/buyer/"+mId);
            Files.createDirectories(path);


            File resource = new File(fakeimages + "/USER/" + (random.nextInt(25) + 1) + ".jpg");
            File destination = new File(images + "/buyer/" + mId + "/" + mId + ".jpg");
            System.out.println("Resource : " + resource.getAbsoluteFile() + "\nDestination : " + destination.getAbsoluteFile());

            if (!resource.exists()) {
                resource = new File(fakeimages + "/USER/1.jpg");
            }
            Files.copy(resource.toPath(), destination.toPath());
        }
        return "Success upload buyer images";
    }

    @CrossOrigin
    @RequestMapping(value = "/dataprepare",method = RequestMethod.GET)
    public String uploadAll() throws IOException, SQLException {
//            InjectUser injectUser = new InjectUser();
//            InjectBuyer injectBuyer = new InjectBuyer();
//            InjectStore injectStore = new InjectStore();
//            InjectTransaction injectTransaction= new InjectTransaction();
//            injectUser.injectUser();
//            injectBuyer.injectBuyer();
//            injectStore.injectStore();
//            injectTransaction.injectTransaction();
            createFolder();
            uploadImage();
            uploadUserImage();
            uploadBuyerImage();

        return "Success upload ALL images";
    }
}