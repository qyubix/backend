package com.backend.backend.datainjector.dao;

import com.backend.backend.dao.ProductDao;
import com.backend.backend.dao.StoreDao;
import com.backend.backend.entity.Product;
import com.backend.backend.entity.Store;
import com.github.javafaker.Faker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Locale;
import java.util.Random;

@RestController
public class InjectProduct {

    @Autowired
    private ProductDao product;

    @Autowired
    private StoreDao store;

    @CrossOrigin
    @RequestMapping(value = "/injectproduct",method = RequestMethod.GET)
    public String injectProduct() {
        int totalData =0;
        System.out.println("Injecting Product table");
        for (Store model:store.findAll()) {
            Random random = new Random();
            int randomVal = (random.nextInt(20)+1);
            for(int i =1; i < randomVal;i++) {
                try {
                    Faker faker = new Faker(new Locale("in-ID"));
                    Product p = new Product();
                    p.setProductCategory(model.getStoreProduct());
                    p.setProductDescription(faker.lorem().sentence(400));
                    p.setProductDiscount(faker.number().numberBetween(1, 70));
                    p.setProductName(faker.commerce().productName());
                    p.setProductPrice(faker.number().numberBetween(5000, 2000000));
                    p.setProductRating(faker.number().numberBetween(1, 10));
                    p.setProductSold(faker.number().numberBetween(1, 9999999));
                    double discounthasil= p.getProductPrice()-(((double)p.getProductDiscount() /100)*p.getProductPrice());
                    p.setProductDiscountAfter((int)discounthasil); ;
                    p.setProductStore(model);
                    p.setProductImageTotal(faker.number().numberBetween(1, 10));
                    product.save(p);
                    totalData++;
                } catch (Exception ex) {
                    continue;
                }
            }
        }
        System.out.println("Finish injected "+totalData+" data to Product table");
        return totalData+" new data injected to Product table";
    }
}