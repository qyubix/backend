package com.backend.backend.datainjector.dao;

import com.backend.backend.dao.BuyerDao;
import com.backend.backend.dao.ProductDao;
import com.backend.backend.dao.TransactionDao;
import com.backend.backend.entity.Buyer;
import com.backend.backend.entity.Product;
import com.backend.backend.entity.Transaction;
import com.github.javafaker.Faker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.management.Query;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Created by AnsariMelah on 06/08/2017.
 */
@RestController
public class InjectTransaction {

    @Autowired
    private BuyerDao buyer;

    @Autowired
    ProductDao product;

    @Autowired
    private TransactionDao transaction;

    @CrossOrigin
    @RequestMapping(value = "/injecttransaction",method = RequestMethod.GET)
    public String injectTransaction() {
        int totalData =0;
        System.out.println("Injecting Transaction table");
        for(int k=0;k<=10000;k++) {
            for (Buyer model : buyer.findAll()) {
                Random random = new Random();
                int randomVal = (random.nextInt(15) + 1);
                for (int i = 1; i < randomVal; i++) {
                    try {
                        Faker faker = new Faker(new Locale("in-ID"));
                        Transaction s = new Transaction();
                        s.setTransactionDate(faker.date().past(999999, TimeUnit.HOURS));
                        s.setTransactionQuantity(faker.number().numberBetween(1, 30));
                        s.setTransactionBuyer(model);
                        int randomstatusVal = (random.nextInt(50) + 1);
                        if (randomstatusVal < 2) {
                            s.setTransactionStatus("booked");
                        } else if (randomstatusVal < 4) {
                            s.setTransactionStatus("onprogress");
                        } else if (randomstatusVal < 5) {
                            s.setTransactionStatus("fail");
                        } else {
                            s.setTransactionStatus("success");
                        }
                        s.setTransactionProduct(product.findRandom());
                        transaction.save(s);
                        totalData++;
                    } catch (Exception ex) {
                        continue;
                    }
                }
            }
        }
        System.out.println("Finish injected "+totalData+" data to transaction table");
        return totalData+" new data injected to Transaction table";
    }
}
