package com.backend.backend.datainjector.dao;

import com.backend.backend.dao.StoreDao;
import com.backend.backend.dao.UserDao;
import com.backend.backend.entity.Store;
import com.backend.backend.entity.User;
import com.github.javafaker.Faker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

@RestController
public class InjectStore {

    @Autowired
    private StoreDao store;


    @Autowired
    private UserDao user;

    @CrossOrigin
    @RequestMapping(value = "/injectstore",method = RequestMethod.GET)
    public String injectStore() {
        int totalData =0;
        System.out.println("Injecting Store table");
        for (User model:user.findAll()) {
                try {
                    Faker faker = new Faker(new Locale("in-ID"));
                    Store s = new Store();
                    s.setStoreKey(faker.company().name().replaceAll("\\W.*", ""));
                    s.setStoreCreated(faker.date().past(9999, TimeUnit.DAYS));
                    s.setStoreDelivery("yes");
                    s.setStoreDescription(faker.lorem().paragraph());
                    s.setStoreLastLogin(faker.date().past(999999, TimeUnit.HOURS));
                    s.setStoreLocation(faker.address().streetAddress());
                    s.setStoreName(faker.commerce().productName());
                    s.setStorePassword(faker.crypto().md5());
                    s.setStoreProduct(faker.commerce().department().replaceAll("\\W.*", ""));
                    s.setStorePhoneNumber(faker.phoneNumber().cellPhone());
                    s.setStoreRisk(faker.number().numberBetween(1, 10));
                    s.setStoreStar(faker.number().numberBetween(1, 10));
                    s.setStoreStatus("active");
                    s.setStoreViewed(faker.number().numberBetween(1, 999999999));
                    s.setStoreUser(model);
                    store.save(s);
                    totalData++;
                } catch (Exception ex) {
                    continue;
                }
        }
        System.out.println("Finish injected  "+totalData+" data to Store table");
        return totalData+" new data injected to Store table";
    }
}
