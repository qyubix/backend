package com.backend.backend.datainjector.dao;

import com.backend.backend.dao.BuyerDao;
import com.backend.backend.entity.Buyer;
import com.github.javafaker.Faker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by AnsariMelah on 08/08/2017.
 */
@RestController
public class InjectBuyer {
    @Autowired
    private BuyerDao buyer;

    @CrossOrigin
    @RequestMapping(value = "/injectbuyer",method = RequestMethod.GET)
    public synchronized String injectBuyer() {
        int totalData =0;
        System.out.println("Injecting Buyer table");
        for(int i = 0; i<100;i++) {
            try {
                Faker faker = new Faker(new Locale("in-ID"));
                Buyer b = new Buyer();
                b.setBuyerBirth(faker.date().past(99999, TimeUnit.DAYS));
                b.setBuyerActiveFrom(faker.date().between(b.getBuyerBirth(), new java.util.Date()));
                b.setBuyerActivity(faker.lorem().paragraph());
                b.setBuyerEmail(faker.internet().emailAddress());
                b.setBuyerLastLogin(faker.date().past(999, TimeUnit.DAYS));
                String name = (faker.name().fullName());
                b.setBuyerName(name.replaceAll("\\W",""));
                b.setBuyerRealName(name);
                b.setBuyerPassword(faker.crypto().md5());
                b.setBuyerStar(faker.number().numberBetween(1,10));
                b.setBuyerStatus("active");
                buyer.save(b);
                totalData++;
            }catch (Exception ex){
                continue;
            }
        }
        System.out.println("Finish injected  "+totalData+" data to Buyer table");
        return totalData+" new data injected to Buyer table";
    }
}
