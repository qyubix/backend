package com.backend.backend.datainjector.dao;

import com.backend.backend.dao.UserDao;
import com.backend.backend.entity.User;
import com.github.javafaker.Faker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

@RestController
public class InjectUser {

    @Autowired
    private UserDao user;

    @CrossOrigin
    @RequestMapping(value = "/injectuser",method = RequestMethod.GET)
    public String injectUser() {
        int totalData =0;
        System.out.println("Injecting User table");
        for(int i = 0; i<100;i++) {
            try {
                Faker faker = new Faker(new Locale("in-ID"));
                User u = new User();
                u.setUserBirth(faker.date().past(99999, TimeUnit.DAYS));
                u.setUserActiveFrom(faker.date().between(u.getUserBirth(), new Date()));
                u.setUserActivity(faker.lorem().paragraph());
                u.setUserEmail(faker.internet().emailAddress());
                u.setUserLastLogin(faker.date().past(999, TimeUnit.DAYS));
                String name = (faker.name().fullName());
                u.setUserName(name.replaceAll("\\W",""));
                u.setUserRealName(name);
                u.setUserPassword(faker.crypto().md5());
                u.setUserStar(faker.number().numberBetween(1,10));
                u.setUserStatus("active");
                user.save(u);
                totalData++;
            }catch (Exception ex){
                continue;
            }
        }
        System.out.println("Finish injected "+totalData+"  data to User table");
        return totalData+" new data injected to User table";
    }
}
