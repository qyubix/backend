package com.backend.backend.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import javax.validation.constraints.Past;
import java.util.Date;
import java.util.List;

/**
 * Created by AnsariMelah on 08/08/2017.
 */

@Entity
public class Buyer
{
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @Column(unique = true)
    private String buyerName;

    private String buyerRealName;

    @Email
    private String buyerEmail;


    private String buyerPassword;

    @Past
    private Date buyerBirth;

    private Date buyerActiveFrom;
    private String buyerStatus;
    private Date buyerLastLogin;
    private String buyerActivity;
    private int buyerStar;

    @OneToMany(mappedBy = "transactionBuyer", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JsonManagedReference(value = "transactionofbuyer")
    private List<Transaction> buyerTransaction;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public String getBuyerEmail() {
        return buyerEmail;
    }

    public void setBuyerEmail(String buyerEmail) {
        this.buyerEmail = buyerEmail;
    }

    public String getBuyerPassword() {
        return buyerPassword;
    }

    public void setBuyerPassword(String buyerPassword) {
        this.buyerPassword = buyerPassword;
    }

    public Date getBuyerBirth() {
        return buyerBirth;
    }

    public void setBuyerBirth(Date buyerBirth) {
        this.buyerBirth = buyerBirth;
    }

    public Date getBuyerActiveFrom() {
        return buyerActiveFrom;
    }

    public void setBuyerActiveFrom(Date buyerActiveFrom) {
        this.buyerActiveFrom = buyerActiveFrom;
    }

    public String getBuyerStatus() {
        return buyerStatus;
    }

    public void setBuyerStatus(String buyerStatus) {
        this.buyerStatus = buyerStatus;
    }

    public Date getBuyerLastLogin() {
        return buyerLastLogin;
    }

    public void setBuyerLastLogin(Date buyerLastLogin) {
        this.buyerLastLogin = buyerLastLogin;
    }

    public String getBuyerActivity() {
        return buyerActivity;
    }

    public void setBuyerActivity(String buyerActivity) {
        this.buyerActivity = buyerActivity;
    }

    public int getBuyerStar() {
        return buyerStar;
    }

    public void setBuyerStar(int buyerStar) {
        this.buyerStar = buyerStar;
    }

    public List<Transaction> getBuyerTransaction() {
        return buyerTransaction;
    }

    public void setBuyerTransaction(List<Transaction> buyerTransaction) {
        this.buyerTransaction = buyerTransaction;
    }

    public String getBuyerRealName() {
        return buyerRealName;
    }

    public void setBuyerRealName(String buyerRealName) {
        this.buyerRealName = buyerRealName;
    }
}

