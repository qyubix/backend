package com.backend.backend.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by AnsariMelah on 01/08/2017.
 */

@Entity
public class Transaction {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference(value = "transactionofproduct")
    private Product transactionProduct;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference(value = "transactionofbuyer")
    private Buyer transactionBuyer;

    private int transactionQuantity;

    private Date transactionDate;

    private String transactionStatus;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Product getTransactionProduct() {
        return transactionProduct;
    }

    public void setTransactionProduct(Product transactionProduct) {
        this.transactionProduct = transactionProduct;
    }

    public Buyer getTransactionBuyer() {
        return transactionBuyer;
    }

    public void setTransactionBuyer(Buyer transactionBuyer) {
        this.transactionBuyer = transactionBuyer;
    }

    public int getTransactionQuantity() {
        return transactionQuantity;
    }

    public void setTransactionQuantity(int transactionQuantity) {
        this.transactionQuantity = transactionQuantity;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }
}
