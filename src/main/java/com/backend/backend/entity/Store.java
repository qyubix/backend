package com.backend.backend.entity;

import com.fasterxml.jackson.annotation.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Entity
public class Store {

    @Id @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference(value = "storeofuser")
    private User storeUser;

    @Column(nullable = false,unique = true)
    @NotNull
    private String storeKey;

    @NotNull
    private String storePassword;

    @NotNull
    private String storeName;

    @NotNull
    private String storeLocation;

    private String storeDescription;

    @NotNull
    private String storePhoneNumber;

    private String storeDelivery;
    private String storeStatus;
    private int storeViewed;

    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date storeCreated;

    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date storeLastLogin;

    private int storeStar;
    private int storeRisk;

    private String storeProduct;

    @OneToMany(mappedBy = "productStore", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JsonManagedReference(value = "productofstore")
    private List<Product> storeProductId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getStoreUser() {
        return storeUser;
    }

    public void setStoreUser(User storeUser) {
        this.storeUser = storeUser;
    }

    public String getStoreKey() {
        return storeKey;
    }

    public void setStoreKey(String storeKey) {
        this.storeKey = storeKey;
    }

    public String getStorePassword() {
        return storePassword;
    }

    public void setStorePassword(String storePassword) {
        this.storePassword = storePassword;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStoreLocation() {
        return storeLocation;
    }

    public void setStoreLocation(String storeLocation) {
        this.storeLocation = storeLocation;
    }

    public String getStoreDescription() {
        return storeDescription;
    }

    public void setStoreDescription(String storeDescription) {
        this.storeDescription = storeDescription;
    }

    public String getStorePhoneNumber() {
        return storePhoneNumber;
    }

    public void setStorePhoneNumber(String storePhoneNumber) {
        this.storePhoneNumber = storePhoneNumber;
    }

    public String getStoreDelivery() {
        return storeDelivery;
    }

    public void setStoreDelivery(String storeDelivery) {
        this.storeDelivery = storeDelivery;
    }

    public String getStoreStatus() {
        return storeStatus;
    }

    public void setStoreStatus(String storeStatus) {
        this.storeStatus = storeStatus;
    }

    public int getStoreViewed() {
        return storeViewed;
    }

    public void setStoreViewed(int storeViewed) {
        this.storeViewed = storeViewed;
    }

    public Date getStoreCreated() {
        return storeCreated;
    }

    public void setStoreCreated(Date storeCreated) {
        this.storeCreated = storeCreated;
    }

    public Date getStoreLastLogin() {
        return storeLastLogin;
    }

    public void setStoreLastLogin(Date storeLastLogin) {
        this.storeLastLogin = storeLastLogin;
    }

    public int getStoreStar() {
        return storeStar;
    }

    public void setStoreStar(int storeStar) {
        this.storeStar = storeStar;
    }

    public int getStoreRisk() {
        return storeRisk;
    }

    public void setStoreRisk(int storeRisk) {
        this.storeRisk = storeRisk;
    }

    public String getStoreProduct() {
        return storeProduct;
    }

    public void setStoreProduct(String storeProduct) {
        this.storeProduct = storeProduct;
    }

    public List<Product> getStoreProductId() {
        return storeProductId;
    }

    public void setStoreProductId(List<Product> storeProductId) {
        this.storeProductId = storeProductId;
    }
}
