package com.backend.backend.entity;

import com.fasterxml.jackson.annotation.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity

public class Product {
    @Id @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference(value = "productofstore")
    private Store productStore;

    @NotNull
    private String productName;

    @NotNull
    private String productCategory;

    @NotNull
    private int productPrice;

    private int productImageTotal;

    @Lob
    @Column(length=10000)
    private String productDescription;

    private int productSold;

    private int productRating;

    private int productDiscount;

    private int productDiscountAfter;

    @OneToMany(mappedBy = "transactionProduct", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JsonManagedReference(value = "transactionofproduct")
    private List<Transaction> productTransactionId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Store getProductStore() {
        return productStore;
    }

    public void setProductStore(Store productStore) {
        this.productStore = productStore;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    public int getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(int productPrice) {
        this.productPrice = productPrice;
    }

    public int getProductImageTotal() {
        return productImageTotal;
    }

    public void setProductImageTotal(int productImageTotal) {
        this.productImageTotal = productImageTotal;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public int getProductSold() {
        return productSold;
    }

    public void setProductSold(int productSold) {
        this.productSold = productSold;
    }

    public int getProductRating() {
        return productRating;
    }

    public void setProductRating(int productRating) {
        this.productRating = productRating;
    }

    public int getProductDiscount() {
        return productDiscount;
    }

    public void setProductDiscount(int productDiscount) {
        this.productDiscount = productDiscount;
    }

    public int getProductDiscountAfter() {
        return productDiscountAfter;
    }

    public void setProductDiscountAfter(int productDiscountAfter) {
        this.productDiscountAfter = productDiscountAfter;
    }

    public List<Transaction> getProductTransactionId() {
        return productTransactionId;
    }

    public void setProductTransactionId(List<Transaction> productTransactionId) {
        this.productTransactionId = productTransactionId;
    }
}
