package com.backend.backend.dao;

import com.backend.backend.entity.Store;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.Date;

public interface StoreDao extends PagingAndSortingRepository<Store,String> {

    @Query("select x from Store x where x.storeCreated = :a")
    public Page<Store> findByStoreCreated(@Param("a") Date storeCreated, Pageable page);

    @Query("select x from Store x where extract(year from x.storeCreated) = :a")
    public Page<Store> findByStoreCreatedYear (@Param("a") int year, Pageable page);

    @Query("select x from Store x where extract(month from x.storeCreated) = :a")
    public Page<Store> findByStoreCreatedMonth (@Param("a") int month, Pageable page);

    @Query("select x from Store x where extract(day from x.storeCreated) = :a")
    public Page<Store> findByStoreCreatedDay (@Param("a") int day, Pageable page);

    @Query("select x from Store x where x.storeDelivery like %:a%")
    public Page<Store> findByStoreDelivery(@Param("a") String storeDelivery, Pageable page);

    @Query("select x from Store x where x.storeDescription like %:a%")
    public Page<Store> findByStoreDescription(@Param("a") String storeDescription, Pageable page);

    @Query("select x from Store x where x.storeKey like %:a%")
    public Page<Store> findByStoreId(@Param("a") String storeId, Pageable page);

    @Query("select x from Store x where x.storeLastLogin = :a")
    public Page<Store> findByStoreLastLogin(@Param("a") Date storeLastLogin, Pageable page);

    @Query("select x from Store x where x.storeLocation like %:a%")
    public Page<Store> findByStoreLocation(@Param("a") String storeLocation, Pageable page);

    @Query("select x from Store x where x.storeName like %:a%")
    public Page<Store> findByStoreName(@Param("a") String storeName, Pageable page);

    @Query("select x from Store x where x.storePhoneNumber = :a")
    public Page<Store> findByStorePhoneNumber(@Param("a") String storePhoneNumber, Pageable page);

    @Query("select x from Store x where x.storeProduct like %:a%")
    public Page<Store> findByStoreProduct(@Param("a") String storeProduct, Pageable page);

    @Query("select x from Store x where x.storeRisk = :a")
    public Page<Store> findByStoreRisk(@Param("a") int storeRisk, Pageable page);

    @Query("select x from Store x where x.storeStar = :a")
    public Page<Store> findByStoreStar(@Param("a") int storeStar, Pageable page);

    @Query("select x from Store x where x.storeStatus = :a")
    public Page<Store> findByStoreStatus(@Param("a") String storeStatus, Pageable page);

    @Query("select x from Store x where x.storeViewed = :a")
    public Page<Store> findByStoreViewed(@Param("a") int storeViewed, Pageable page);

    @Query("select storeProduct from Store where storeKey = :a")
    public String findStoreProductByStoreKey(@Param("a") String storeKey);

    @Query("select id from Store where storeUserId = :a")
    public Page<Store> findStoreProductByStoreProductId(@Param("a") String productId, Pageable page);
}
