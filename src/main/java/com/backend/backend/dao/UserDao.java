package com.backend.backend.dao;

import com.backend.backend.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserDao extends JpaRepository<User,String> {
    @Query(value = "select " +
            "id," +
            "user_active_from," +
            "user_activity," +
            "user_birth,user_email," +
            "user_last_login," +
            "user_name," +
            "user_real_name," +
            "user_role," +
            "user_star," +
            "user_status from user limit :limit"
            ,nativeQuery = true)
    public List[] findAllUser(@Param("limit") int limit);

    @Query(value = "select user.id as 'user_id'," +
            "user.user_real_name, " +
            "sum(transaction.transaction_quantity*product.product_price) as 'total_price' , " +
            "sum(transaction.transaction_quantity*product.product_discount_after) as 'total_after_discount' , " +
            "(sum(transaction.transaction_quantity*product.product_price))-(sum(transaction.transaction_quantity*product.product_discount_after)) as 'total_discount'" +
            "from user join store on user.id = store.store_user_id " +
            "join product on store.id = product.product_store_id " +
            "join transaction on product.id = transaction.transaction_product_id " +
            "group by user_name   ORDER BY total_discount   DESC  limit :limit"
            ,nativeQuery = true)
    public List[] findTheMostDiscountGiver(@Param("limit") int limit);

    @Query(value = "select " +
            "id," +
            "user_active_from," +
            "user_activity," +
            "user_birth," +
            "user_email," +
            "user_last_login," +
            "user_name," +
            "user_real_name," +
            "user_role," +
            "user_star," +
            "user_status from user where id=:id"
            ,nativeQuery = true)
    public List[] findUserById(@Param("id") String id);

    @Query("select x.id from User x where x.userName = :a and x.userPassword=:b")
    public String findIdByUserNameAndPassword(@Param("a") String username, @Param("b") String password);
}
