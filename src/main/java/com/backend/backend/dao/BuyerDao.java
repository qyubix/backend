package com.backend.backend.dao;

import com.backend.backend.entity.Buyer;
import com.backend.backend.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

/**
 * Created by AnsariMelah on 08/08/2017.
 */
public interface BuyerDao extends PagingAndSortingRepository<Buyer,String> {
    @Query("select x from Buyer x where x.buyerName = :a")
    public Buyer findByBuyerName(@Param("a") String buyername);

    @Query("select x.id from Buyer x where x.buyerName = :a and x.buyerPassword=:b")
    public String findIdByUserNameAndPassword(@Param("a") String username, @Param("b") String password);
}
