package com.backend.backend.dao;

import com.backend.backend.entity.Product;
import com.backend.backend.entity.Store;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import javax.persistence.EntityManager;
import java.util.List;

public interface ProductDao extends PagingAndSortingRepository<Product,String> {
    @Query(value = "select product.*, store.store_key from product left join store on product.product_store_id =" +
            " store.id where product.product_name like %:product_name% limit :limit",nativeQuery = true)
    public List[] findProductByProductNameWithPagination(@Param("product_name") String productCategory,
                                                         @Param("limit") int limit);

    @Query(value = "select product.*, store.store_key from product left join store on product.product_store_id =" +
            " store.id where product.product_category like %:a% limit :limit",nativeQuery = true)
    public List[] findComplete(@Param("a") String productCategory,
                               @Param("limit") int limit);

    @Query(value = "select * from Product ORDER BY rand() LIMIT 1",nativeQuery = true)
    public Product findRandom();

    @Query(value = "select product.*, store.store_key from product left join store on " +
            "product.product_store_id = store.id where product.id= :a",nativeQuery = true)
    public List[] findCompleteById(@Param("a") String productId);


    @Query(value = "select product.id, product.product_name, product.product_price, store.store_key " +
            "from product left join store on product.product_store_id = store.id where product.product_category " +
            "like %:product_category% limit :limit",nativeQuery = true)
    public List[] findIdProductNameProductPriceStoreKey(@Param("product_category") String productCategory,
                                                        @Param("limit") int limit);

    @Query(value = "select product.id,product.product_name, store.store_key from product  left join store on " +
            "product.product_store_id = store.id limit :limit",nativeQuery = true)
    public List[] findIdNameStore(@Param("limit") int limit);

//    @Query(value = "select * from product",nativeQuery = true)
//    public List[] findCompleteWithoutLimit();
//
//    public Page<Product> findTop9ByProductCategory(String productcategory,Pageable page);
//
//    @Query(value = "select x from Product x where x.productCategory like %:a%")
//    public Page<Product> findByProductCategory(@Param("a") String productCategory, Pageable page);
//
//    @Query("select x from Product x where x.productDescription like %:a%")
//    public Page<Product> findByProductDescription(@Param("a") String productDescription, Pageable page);
//
//    @Query("select x from Product x where x.productDiscount = :a")
//    public Page<Product> findByProductDiscount(@Param("a") int productDiscount, Pageable page);
//
//    @Query("select x from Product x where x.productDiscountAfter = :a")
//    public Page<Product> findByProductDiscountAfter(@Param("a") int productDiscountAfter, Pageable page);
//
//    @Query("select x,  x.productStore.id  from Product x where x.productStore.id like %:a%")
//    public Page<String> findByProductStoreId(@Param("a") String id, Pageable page);
//
//    @Query("select x from Product x where x.productName like %:a%")
//    public Page<Product> findByProductName(@Param("a") String productName, Pageable page);
//
//    @Query("select x from Product x where x.productPrice = :a")
//    public Page<Product> findByProductPrice(@Param("a") int productPrice, Pageable page);
//
//    @Query("select x from Product x where x.productRating = :a")
//    public Page<Product> findByProductRating(@Param("a") int productRating, Pageable page);
//
//    @Query("select x from Product x where x.productSold = :a")
//    public Page<Product> findByProductSold(@Param("a") int productSold, Pageable page);
//
//    @Query("select x from Product x where x.productImageTotal = :a")
//    public Page<Product> findByProductImageTotal(@Param("a") int productImageTotal, Pageable page);
//
//    @Query("select x.id from Product x join x.productStore a where a.storeKey=:key")
//    public Store getStoreProductByStoreKey(@Param("key") String storeKey);
//
//
//
//

}
