package com.backend.backend.dao;

import com.backend.backend.entity.Transaction;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

/**
 * Created by AnsariMelah on 01/08/2017.
 */
public interface TransactionDao extends PagingAndSortingRepository<Transaction, String > {
    @Query(value = "select productresult.id as 'product_id', " +
            "transaction.id as 'transaction_id', " +
            "transaction.transaction_date, " +
            "transaction.transaction_quantity, " +
            "transaction.transaction_status," +
            "transaction.transaction_buyer_id from transaction " +
            "JOIN (select product.id from product " +
            "JOIN store on product.product_store_id = store.id " +
            "JOIN user on store.store_user_id = user.id " +
            "where user.id=:user_id GROUP BY product.id) " +
            "AS productresult on productresult.id= transaction.transaction_product_id " +
            "GROUP BY transaction.id limit :limit",nativeQuery = true)
    public List[] findTransactionByUserId(
            @Param("user_id") String userId,
            @Param("limit") int limit);

    @Query(value = "select productresult.id as 'product_id', " +
            "productresult.product_store_id, " +
            "productresult.store_key, " +
            "productresult.product_category, " +
            "productresult.product_description," +
            "productresult.product_discount, " +
            "productresult.product_discount_after, " +
            "productresult.product_image_total, " +
            "productresult.product_name, " +
            "productresult.product_price, " +
            "productresult.product_rating, " +
            "productresult.product_sold, " +
            "transaction.id as 'transaction_id', " +
            "transaction.transaction_date, " +
            "transaction.transaction_quantity, " +
            "transaction.transaction_status," +
            "transaction.transaction_buyer_id from transaction " +
            "JOIN (select product.*, store.store_key from product " +
            "JOIN store on product.product_store_id = store.id " +
            "JOIN user on store.store_user_id = user.id " +
            "where user.id=:user_id GROUP BY product.id) " +
            "AS productresult on productresult.id= transaction.transaction_product_id " +
            "where transaction.transaction_status=:transaction_status " +
            "GROUP BY transaction.id limit :limit",nativeQuery = true)
    public List[] findTransactionAndProductAndStoreKeyByUserIdAndTransactionStatus(
            @Param("user_id") String userId,
            @Param("transaction_status") String transactionStatus,
            @Param("limit") int limit);


    @Query(value = "select productresult.*, " +
            "transaction.id as 'transaction_id', " +
            "transaction.transaction_buyer_id as 'buyer_id' " +
            "from transaction JOIN " +
            "(select product.id as 'product_id', " +
            "store.id as 'store_id' from product " +
            "JOIN store on product.product_store_id = store.id " +
            "JOIN user on store.store_user_id = user.id " +
            "where user.id=:user_id GROUP BY product.id) " +
            "AS productresult on productresult.product_id= transaction.transaction_product_id " +
            "GROUP BY transaction.id",nativeQuery = true)
    public List[] findTransactionWithAllRelationshipIdByUserId(
            @Param("user_id") String userId);

    @Query(value = "select productresult.*, " +
            "transaction.id as 'transaction_id'," +
            "transaction.transaction_date," +
            "transaction.transaction_quantity," +
            "transaction.transaction_status," +
            "transaction.transaction_buyer_id as 'buyer_id' from transaction JOIN " +
            "(select product.id as 'product_id', " +
            "        product.product_category, " +
            "        product.product_description," +
            "        product.product_discount, " +
            "        product.product_discount_after, " +
            "        product.product_image_total, " +
            "        product.product_name, " +
            "        product.product_price, " +
            "        product.product_rating, " +
            "        product.product_sold, " +
            "store.id as 'store_id', " +
            "store.store_created," +
            "store.store_delivery," +
            "store.store_description," +
            "store.store_key," +
            "store.store_last_login," +
            "store.store_location," +
            "store.store_name," +
            "store.store_phone_number," +
            "store.store_risk, " +
            "store.store_star," +
            "store.store_status," +
            "store.store_viewed from product " +
            "            JOIN store on product.product_store_id = store.id " +
            "            JOIN user on store.store_user_id = user.id " +
            "            where user.id=:user_id GROUP BY product.id) " +
            "            AS productresult on productresult.product_id= transaction.transaction_product_id " +
            "            GROUP BY transaction.id",nativeQuery = true)
    public List[] findTransactionWithAllRelationshipByUserId(@Param("user_id") String userId);


    @Query(value = "INSERT INTO transaction (id," +
            "transaction_date, " +
            "transaction_quantity, " +
            "transaction_status, " +
            "transaction_buyer_id," +
            "transaction_product_id)\n" +
            "VALUES (:id,:date, :quantity, :status, :buyer_id,:product_id)",nativeQuery = true)
    public List[] insertNewTransaction(
            @Param("id") String id,
            @Param("date") Date date,
            @Param("quantity") int quantity,
            @Param("status") String status,
            @Param("buyer_id") String buyerId,
            @Param("product_id") String productId
    );
}
