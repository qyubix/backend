package com.backend.backend.json;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.scheduling.annotation.Async;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by AnsariMelah on 15/08/2017.
 */
public class JsonProduct {

    @Async
    public JSONObject getFullProduct(List[] listProducts){
        ArrayList<String> columnFlag = new ArrayList<String>();
        columnFlag.add("id");
        columnFlag.add("product_category");
        columnFlag.add("product_description");
        columnFlag.add("product_discount");
        columnFlag.add("product_discount_after");
        columnFlag.add("product_image_total");
        columnFlag.add("product_name");
        columnFlag.add("product_price");
        columnFlag.add("product_rating");
        columnFlag.add("product_sold");
        columnFlag.add("product_store_id");
        columnFlag.add("product_store_key");

        JSONObject objectProduct = JsonMapper.jsonMapper(columnFlag,listProducts,0,0);
        return objectProduct;
    }

    @Async
    public JSONObject getIdProductNameProductPriceStoreKey(List[] listProducts){
        ArrayList<String> columnFlag = new ArrayList<String>();
        columnFlag.add("id");
        columnFlag.add("product_name");
        columnFlag.add("product_price");
        columnFlag.add("product_store_key");

        JSONObject objectProduct = JsonMapper.jsonMapper(columnFlag,listProducts,0,0);
        return objectProduct;
    }

    @Async
    public JSONObject getIdNameStore(List[] listProducts){
        ArrayList<String> columnFlag = new ArrayList<String>();
        columnFlag.add("id");
        columnFlag.add("product_name");
        columnFlag.add("product_store_key");

        JSONObject objectProduct = JsonMapper.jsonMapper(columnFlag,listProducts,0,0);
        return objectProduct;
    }

    @Async
    public JSONObject getFullProductWithPagination(List[] listProducts,int size, int number){
        ArrayList<String> columnFlag = new ArrayList<String>();
        columnFlag.add("id");
        columnFlag.add("product_category");
        columnFlag.add("product_description");
        columnFlag.add("product_discount");
        columnFlag.add("product_discount_after");
        columnFlag.add("product_image_total");
        columnFlag.add("product_name");
        columnFlag.add("product_price");
        columnFlag.add("product_rating");
        columnFlag.add("product_sold");
        columnFlag.add("product_store_id");
        columnFlag.add("product_store_key");

        JSONObject objectProduct = JsonMapper.jsonMapper(columnFlag,listProducts,size,number);
        return objectProduct;
    }
}
