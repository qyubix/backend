package com.backend.backend.json;

import org.json.simple.JSONObject;
import org.springframework.scheduling.annotation.Async;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by AnsariMelah on 21/08/2017.
 */
public class JsonUser {
    @Async
    public JSONObject getAllUser(List[] listProducts){
        ArrayList<String> columnFlag = new ArrayList<String>();
        columnFlag.add("id");
        columnFlag.add("user_active_from");
        columnFlag.add("user_activity");
        columnFlag.add("user_birth");
        columnFlag.add("user_email");
        columnFlag.add("user_last_login");
        columnFlag.add("user_name");
        columnFlag.add("user_real_name");
        columnFlag.add("user_role");
        columnFlag.add("user_star");
        columnFlag.add("user_status");

        JSONObject objectProduct = JsonMapper.jsonMapper(columnFlag,listProducts,0,0);
        return objectProduct;
    }

    @Async
    public JSONObject findTheMostDiscountGiver(List[] listProducts){
        ArrayList<String> columnFlag = new ArrayList<String>();
        columnFlag.add("user_id");
        columnFlag.add("user_real_name");
        columnFlag.add("total_price");
        columnFlag.add("total_after_discount");
        columnFlag.add("total_discount");

        JSONObject objectProduct = JsonMapper.jsonMapper(columnFlag,listProducts,0,0);
        return objectProduct;
    }
}
