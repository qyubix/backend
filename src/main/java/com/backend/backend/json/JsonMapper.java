package com.backend.backend.json;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.scheduling.annotation.Async;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by AnsariMelah on 18/08/2017.
 *
 * Input :
 * -ArrayList<String> columnFlag : column which will be mapped from database. this will be key of the json
 * -List[] listItems : This list item is result of query from database, this will be mapped together with
 *                     columnFlag as value of the json
 * -size : a number of item per page
 * -number : current page number
 */
public class JsonMapper {
    @Async
    public static JSONObject jsonMapper(ArrayList<String> columnFlag, List[] listItems, int size, int number){
        JSONObject objectItem =new JSONObject();
        int objNumber=0;
        int firstIndexCurrentPage =0;
        int lastIndexCurrentPage = 0;

        //Json can be defined as pagination if either size or number is not 0
        boolean pagination = (size!=0 || number!=0) ? true : false ;


        /* If the pagination true, then this method will add last, number, totalPages,
         * totalelements and size to Json, if not, then json just have content which
         * will contain an array of items
         * */
        if(pagination){
            firstIndexCurrentPage =(size*number)+1;
            lastIndexCurrentPage = firstIndexCurrentPage+size-1;

            //check how many pages require to contain the list of content
            int numberOfPage = listItems.length/size;

            //if the page still remain other element to contain, then wi add 1 more page
            if(listItems.length%size!=0){
                numberOfPage = numberOfPage+1;
            }

            objectItem.put("last",numberOfPage==number?"true":"false");
            objectItem.put("number",number);
            objectItem.put("totalPages",numberOfPage);
            objectItem.put("totalelements",listItems.length);
            objectItem.put("size",size);
        }

        // this is the main logic which mapped list[] with columnFlag to be objectItem of Json
        JSONArray dataSet = new JSONArray();
        for (List listItem:listItems) {
            objNumber++;
            if( (objNumber>=firstIndexCurrentPage && objNumber<=lastIndexCurrentPage) || !pagination){
                JSONObject content = new JSONObject();
                for (String columnVal:columnFlag) {
                    content.put(columnVal,listItem.get(columnFlag.indexOf(columnVal))) ;
                }
                dataSet.add(content);
            }
        }
        objectItem.put("content",dataSet);
        return objectItem;
    }
}
