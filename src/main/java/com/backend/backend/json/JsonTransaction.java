package com.backend.backend.json;

import org.json.simple.JSONObject;
import org.springframework.scheduling.annotation.Async;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by AnsariMelah on 20/08/2017.
 */
public class JsonTransaction {
    @Async
    public JSONObject getTransactionAndProductAndStoreKeyByUserIdAndTransactionStatus(List[] listProducts){
        JSONObject objectProduct = JsonMapper.jsonMapper(getAllColumn(),listProducts,0,0);
        return objectProduct;
    }

    @Async
    public JSONObject getTransactionWithAllRelationshipIdByUserId(List[] listProducts){
        ArrayList<String> columnFlag = new ArrayList<String>();
        columnFlag.add("product_id");
        columnFlag.add("store_id");
        columnFlag.add("transaction_id");
        columnFlag.add("buyer_id");

        JSONObject objectProduct = JsonMapper.jsonMapper(columnFlag,listProducts,0,0);
        return objectProduct;
    }

    @Async
    public JSONObject getTransactionWithAllRelationshipByUserId(List[] listProducts){
        ArrayList<String> columnFlag = new ArrayList<String>();
        columnFlag.add("product_id");
        columnFlag.add("product_category");
        columnFlag.add("product_description");
        columnFlag.add("product_discount");
        columnFlag.add("product_discount_after");
        columnFlag.add("product_image_total");
        columnFlag.add("product_name");
        columnFlag.add("product_price");
        columnFlag.add("product_rating");
        columnFlag.add("product_sold");
        columnFlag.add("store_id");
        columnFlag.add("store_created");
        columnFlag.add("store_delivery");
        columnFlag.add("store_description");
        columnFlag.add("store_key");
        columnFlag.add("store_last_login");
        columnFlag.add("store_location");
        columnFlag.add("store_name");
        columnFlag.add("store_phone_number");
        columnFlag.add("store_risk");
        columnFlag.add("store_star");
        columnFlag.add("store_status");
        columnFlag.add("store_viewed");
        columnFlag.add("transaction_id");
        columnFlag.add("transaction_date");
        columnFlag.add("transaction_quantity");
        columnFlag.add("transaction_status");
        columnFlag.add("buyer_id");

        JSONObject objectProduct = JsonMapper.jsonMapper(columnFlag,listProducts,0,0);
        return objectProduct;
    }

    @Async
    public JSONObject getTransactionAndProductAndStoreKeyByUserIdAndTransactionStatusWithPagination
            (List[] listProducts,int size,int number){
        JSONObject objectProduct = JsonMapper.jsonMapper(getAllColumn(),listProducts,size,number);
        return objectProduct;
    }

    private ArrayList<String> getAllColumn(){
        ArrayList<String> columnFlag = new ArrayList<String>();
        columnFlag.add("product_id");
        columnFlag.add("store_id");
        columnFlag.add("store_key");
        columnFlag.add("product_category");
        columnFlag.add("product_description");
        columnFlag.add("product_discount");
        columnFlag.add("product_discount_after");
        columnFlag.add("product_image_total");
        columnFlag.add("product_name");
        columnFlag.add("product_price");
        columnFlag.add("product_rating");
        columnFlag.add("product_sold");
        columnFlag.add("transaction_id");
        columnFlag.add("transaction_date");
        columnFlag.add("transaction_quantity");
        columnFlag.add("transaction_status");
        columnFlag.add("transaction_buyer_id");
        return columnFlag;
    }
}
